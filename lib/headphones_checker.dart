import 'dart:async';

import 'package:flutter/services.dart';

class HeadphonesChecker {
  static const MethodChannel _channel =
      const MethodChannel('headphones_checker');

  static Future<bool> get areHeadphonesPluggedIn async {
    final bool areHeadphonesPluggedIn = await _channel.invokeMethod('areHeadphonesPluggedIn');
    return areHeadphonesPluggedIn;
  }
}
