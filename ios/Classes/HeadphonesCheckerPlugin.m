#import "HeadphonesCheckerPlugin.h"
#import <headphones_checker/headphones_checker-Swift.h>

@implementation HeadphonesCheckerPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftHeadphonesCheckerPlugin registerWithRegistrar:registrar];
}
@end
