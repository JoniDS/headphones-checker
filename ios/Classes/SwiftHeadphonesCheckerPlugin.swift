import Flutter
import UIKit
import AVFoundation


public class SwiftHeadphonesCheckerPlugin: NSObject, FlutterPlugin {
  public static func register(with registrar: FlutterPluginRegistrar) {
    let channel = FlutterMethodChannel(name: "headphones_checker", binaryMessenger: registrar.messenger())
    let instance = SwiftHeadphonesCheckerPlugin()
    registrar.addMethodCallDelegate(instance, channel: channel)
  }
    
    func headsetPluggedIn() -> Bool {
        let route = AVAudioSession.sharedInstance().currentRoute
        return (route.outputs as! [AVAudioSessionPortDescription]).filter({ $0.portType == AVAudioSessionPortHeadphones }).count > 0
    }

  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
    result(headsetPluggedIn())
  }
}
