package com.jds.headphones_checker.headphones_checker

import android.content.Context
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.PluginRegistry.Registrar
import android.media.AudioManager
import android.media.AudioDeviceInfo
import android.os.Build


class HeadphonesCheckerPlugin(private val context: Context) : MethodCallHandler {
    companion object {
        @JvmStatic
        fun registerWith(registrar: Registrar) {
            val channel = MethodChannel(registrar.messenger(), "headphones_checker")
            channel.setMethodCallHandler(HeadphonesCheckerPlugin(registrar.context()))
        }
    }

    private fun areHeadphonesPlugged(): Boolean {
        val audioManager = context.getSystemService(Context.AUDIO_SERVICE) as AudioManager
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val audioDevices = audioManager.getDevices(AudioManager.GET_DEVICES_ALL)
            audioDevices?.firstOrNull { deviceInfo -> deviceInfo.type == AudioDeviceInfo.TYPE_WIRED_HEADPHONES || deviceInfo.type == AudioDeviceInfo.TYPE_WIRED_HEADSET } != null
        } else {
            audioManager.isWiredHeadsetOn || audioManager.isBluetoothA2dpOn
        }
    }

    override fun onMethodCall(call: MethodCall, result: Result) {
        if (call.method == "areHeadphonesPluggedIn") {
            result.success(areHeadphonesPlugged())
        } else {
            result.notImplemented()
        }
    }
}
