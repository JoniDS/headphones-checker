import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:headphones_checker/headphones_checker.dart';

void main() {
  const MethodChannel channel = MethodChannel('headphones_checker');

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return false;
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('areHeadphonesPluggedIn', () async {
    expect(await HeadphonesChecker.areHeadphonesPluggedIn, false);
  });
}
